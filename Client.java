
public class Client implements Comparable<Client> {
	private int clientId;
	private int arrivalTime;
	private int processingTime;
	
	public Client (int timeArrival, int servingTime) {
		this.clientId = 0;
		this.arrivalTime = timeArrival;
		this.processingTime = servingTime;
	}
	
	public void setClientId(int id) {
		this.clientId = id;
	}
	
	public Client() {
		
	}
	
	public int getArrivalTime() {
		return this.arrivalTime;
	}
	
	public void setProcessingTime(int val) {
		this.processingTime = val;
	}
	
	public int getProcessingTime() {
		return this.processingTime;
	}
	
	public int getId() {
		return this.clientId;
	}
	
	public int compareTo(Client c) {
		if (this.getArrivalTime() > c.getArrivalTime())
			return 1;
		if(this.getArrivalTime() > c.getArrivalTime())
			return 0;
		return -1;
	}
}

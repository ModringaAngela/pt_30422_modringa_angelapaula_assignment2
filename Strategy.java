import java.util.List;

public interface Strategy {
	
	public void addClients(List<Server> servers, Client t);
}

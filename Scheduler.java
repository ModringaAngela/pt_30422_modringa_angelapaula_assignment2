import java.util.*;
public class Scheduler {
	
	private List<Server> servers = new ArrayList<Server>();
	private ConcreteStrategyTime strategy;
	private int totalWaitingTime;
	
	public Scheduler(int nrOfServers) {
		
		for(int i = 0 ; i < nrOfServers ; i++) {
			
			Server newServer = new Server(i+1);
			servers.add(newServer);
			Thread t = new Thread(newServer);
			t.start();
			
		}
		this.strategy = new ConcreteStrategyTime();
	}
	
	public synchronized void dispatchClient(Client c) {
		this.strategy.addClients(servers, c);
	}
	
	public List<Server> getServers(){
		return this.servers;
	}
	
	public void setTotalWaitingTime() {
		this.totalWaitingTime = this.strategy.getTotalWaitingTime();
	}
	
	public int getTotalWaitingTime() {
		return this.totalWaitingTime;
	}

}
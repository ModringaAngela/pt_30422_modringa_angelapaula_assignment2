import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	
	private BlockingQueue<Client> clientsWaitingHere;
	private AtomicInteger waitingPeriod;
	private int serverId;
	
	public Server(){
		this.serverId = 0; 
	}
	
	public Server(int id) {
		
		this.serverId = id;
		this.clientsWaitingHere = new LinkedBlockingQueue<Client>();
		this.waitingPeriod = new AtomicInteger(0);
	}
	
	public void addClient(Client newClient){ 
		try {
			this.clientsWaitingHere.add(newClient);
			int processingTime = newClient.getProcessingTime();
			waitingPeriod.set(waitingPeriod.get() + processingTime);
		}
		catch(NullPointerException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public BlockingQueue<Client> getClients() {
		return this.clientsWaitingHere;
	}

	public int getWaitingPeriod() {
		return this.waitingPeriod.get();  
	}

	public int getServerId() {
		return this.serverId;
	}
	
	public void run() {
		
		while(true) {
			Client currentClient = new Client();
		try {
				currentClient = clientsWaitingHere.element();
		}
		catch(NoSuchElementException e) {
			continue;
		}
			
		try {
			int processingTime = currentClient.getProcessingTime();
			for(int i = 0 ; i < processingTime ; i++) {
				Thread.sleep(1000);
				clientsWaitingHere.element().setProcessingTime(clientsWaitingHere.element().getProcessingTime() - 1 );
			}
			clientsWaitingHere.remove();
			waitingPeriod.set(waitingPeriod.get() - processingTime);
		}
		catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
		}
	}
}

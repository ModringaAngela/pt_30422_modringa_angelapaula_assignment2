import java.util.List;

public class ConcreteStrategyTime implements Strategy{
	
	private int totalWaitingTime = 0;
	
	public ConcreteStrategyTime() {
	}

	@Override
	public void addClients(List<Server> servers, Client c) {
		
		Server assignedServer = new Server();
		int minWaitingTime = Integer.MAX_VALUE;
		int waitingTimeAtThisServer;
		
		for(Server s : servers) {
			waitingTimeAtThisServer = s.getWaitingPeriod();
			if( waitingTimeAtThisServer < minWaitingTime) {
				minWaitingTime = waitingTimeAtThisServer;
				assignedServer = s;
			}
		}
		assignedServer.addClient(c);
		totalWaitingTime += assignedServer.getWaitingPeriod();
	}
	
	public int getTotalWaitingTime() {
		return this.totalWaitingTime;
	}

}
